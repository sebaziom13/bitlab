import socket
import struct
import time
import random
import codecs
import hashlib
import os


# nslookup seed.bitcoin.sipa.be
# DNS:  https://en.bitcoin.it/wiki/Satoshi_Client_Node_Discovery
# dokumentacja bitcoin: https://en.bitcoin.it/wiki/Protocol_documentation#getheaders

class Version:
    version = struct.pack("i", 70015)  # Wersja protoko?u, jak? si? komunikujemy. W naszym przypadku jest to 70002
    services = struct.pack("Q",0)  #  Przyjmiemy tutaj na sta?e warto?? 0, kt?ra oznacza, ?e aplikacja nie posiada pe?ne bloki zawieraj?ce histori? transakcji sieci i inne aplikacje nie mog? o te bloki wys?a? zapytanie.
    timestamp = struct.pack("q", int(time.time()))  # Aktualny czas w formacie uniksowym na komputerze wysy?aj?cym wiadomo?? .
    addr_recv_services = struct.pack("Q", 0)
    addr_recv_ip = struct.pack(">16s", bytes("52.144.47.153", 'utf-8'))  # adres sieci bitcoin
    addr_recv_port = struct.pack(">H", 8333)  # 8333 is a default port for bitcoin protocol
    addr_trans_services = struct.pack("Q", 0)
    addr_trans_ip = struct.pack(">16s", bytes("127.0.0.1", 'utf-8'))  # > - big endian, 16 - sixteen, s - characters
    addr_trans_port = struct.pack(">H", 8333)  # 8333 is a default port for bitcoin protocol
    nonce = struct.pack("Q", random.getrandbits(64))  # Liczba losowa identyfikuj?ca konkretny w?ze?. Je?eli warto?? tego pola wynosi 0, jego znaczenie jest ignorowane
    user_agent_bytes = struct.pack("B",0)  # Nazwa aplikacji obs?uguj?cej protok?. Pierwsze bajty okre?laj? d?ugo?? nazwy, nast?pnie po nich wyst?puje ci?g znak?w w formacie ASCII.
    starting_height = struct.pack("i", 0)  # Numer najnowszego posiadanego bloku transakcji
    relay = struct.pack("?", False)


class Naglowek:
    magic = b'\0' * 4
    command = b'\0' * 12
    length = b'\0' * 4
    checksum = b'\0' * 4

class Verack:
    command = "verack" + (12 - len("verack")) * "\00"
    payload = str.encode("")
    suma_kontrolna = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]
    length = struct.pack("I", len(payload))


class Payload:
    header = Naglowek()
    payload = b'\0'



def analizanaglowka(input):  #analizowanie kazdego nag??wka, kt?ry odbierze program

    h = Naglowek()
    h.magic = input[:4]
    h.command = input[4:16]
    command_actual_length = 0
    for i in h.command:
        if (i != 0):
            command_actual_length += 1
        else:
            break
    command_in_str = h.command[:command_actual_length].decode("utf-8")
    h.command = command_in_str

    print("---------------Odebranie pakietu---------------------\n")

    h.length = input[16:20]
    h.length = int.from_bytes(h.length, byteorder='little')
    h.checksum = input[20:]
    print("Magic:", h.magic.hex())
    print("Command:", h.command)
    print("Payload Lenght:",h.length )
    print("Checksum:",h.checksum.hex())
    return h


def VarInt(Bit): #Im wi?ksza liczba, tym wi?cej bajt?w zajmuje (1-9)
    Start = -1
    koniec = -1
    if (Bit == 253):  # 0xfd, I
        print("type uint16_t")
        Start = 1
        koniec = 3
    else:
        if (Bit == 254):  # 0xfe
            print("type uint32_t")
            Start = 1
            koniec = 5
        else:
            if (Bit == 255):  # 0xff
                print("type uint64_t")
                Start = 1
                koniec = 9
            else:
                print("type uint8_t")
                Start = 0
                koniec = 1

    iteracja = [Start, koniec]
    return iteracja


def blockTypeIdentifier(typeIdentifier): #typ bloku
    if (typeIdentifier == 1):
        return "MSG_TX"
    if (typeIdentifier == 2):
        return "MSG_BLOCK"
    if (typeIdentifier == 3):
        return "MSG_FILTERED_BLOCK"
    if (typeIdentifier == 4):
        return "MSG_CMPCT_BLOCK"
    if (typeIdentifier == 5):
        return "MSG_WITNESS_BLOCK"
    if (typeIdentifier == 6):
        return "MSG_WITNESS_TX"
    if (typeIdentifier == 7):
        return "MSG_FILTERED_WITNESS_BLOCK"


myBlockHeaders = []
myBlockHeadersInBytes = []

def Odczyt_naglowka_z_socketu(socket):
    naglowek = socket.recv(24)
    return naglowek


def payloadInterpreter(payload, commandName): # odczyt payloadu
    payloadStr = []
    if (commandName == 'addr'):
        Bit = payload[0]
        iteration = VarInt(Bit)
        numberOfAddresses = int.from_bytes(payload[iteration[0]:iteration[1]], byteorder='little')
        payloadStr.append("Liczba adres?w: " + str(numberOfAddresses))
        addressBegin = iteration[1]
        addressEnd = iteration[1] + 30
        while addressEnd <= len(payload):
            address = payload[addressBegin:addressEnd]
            payloadStr.append("IP: " +address[12:14].hex() + ":" +address[14:16].hex() + ":" +address[16:18].hex() + ":" +address[18:20].hex() + ":" +address[20:22].hex() + ":" +address[22:24].hex() + ":" +address[24:26].hex() + ":" +address[26:28].hex() + ":" + ", " +"Port: " + str(int.from_bytes(address[28:30], byteorder='big')))
            addressBegin = addressEnd
            addressEnd += 30
    else:
        if (commandName == 'inv'):
            Bit = payload[0]
            iteration = VarInt(Bit)
            numberOfBlocks = int.from_bytes(payload[iteration[0]:iteration[1]], byteorder='little')
            payloadStr.append("Liczba blok?w: " + str(numberOfBlocks))
            blockBegin = iteration[1]
            blockLength = 36
            blockEnd = iteration[1] + blockLength
            for i in range(numberOfBlocks):
                block = payload[blockBegin:blockEnd]
                myBlockHeadersInBytes.append(block)
                payloadStr.append("Typ bloku: " + blockTypeIdentifier(int.from_bytes(block[0:4], byteorder='little')) + ", Warto?? Hash: " + block[4:36].hex())
                blockBegin = blockEnd
                blockEnd += blockLength
        else:
            if (commandName == 'headers'):
                Bit = payload[0]
                iteration = VarInt(Bit)
                numberOfHeaders = int.from_bytes(payload[iteration[0]:iteration[1]], byteorder='little')
                payloadStr.append("Liczba nag?owk?w: " + str(numberOfHeaders))
                headerBegin = iteration[1]
                headerLength = 80
                headerEnd = iteration[1] + headerLength
                for i in range(numberOfHeaders):
                    header = payload[headerBegin:headerEnd]
                    payloadStr.append("Version: " + str(int.from_bytes(header[0:4], byteorder='little')) + ", " + "Poprzedni hash bloku: " + header[4:36].hex() + ", " + "Hash bloku: " + header[36:68].hex() + ", " + "Nonce: " + str(int.from_bytes(header[76:80], byteorder='little')) )
                    headerBegin = headerEnd
                    headerEnd += headerLength
            else:
                if (commandName == 'ping'):
                    payloadStr.append("Nonce: " + payload.hex())
                payloadStr.append("")

    return payloadStr


def Pobierz_adresy_wezlow(socket):
    magic = codecs.decode('F9BEB4D9', 'hex')
    command = "getaddr" + (12 - len("getaddr")) * "\00"
    payload = str.encode("")
    length = struct.pack("I", len(payload))
    suma_kontrolna = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]
    pakiet_danych = magic + str.encode(command) + length + suma_kontrolna + payload
    socket.send(pakiet_danych)


def Pobierz_bloki(socket, magic, version, firstBlockHash):
    commandName = "getblocks"
    command = commandName + (12 - len(commandName)) * "\00"
    hashCount = struct.pack("B", 1)
    blockHeaderHashes = firstBlockHash
    stopHash = b'\0' * 32
    payload = version + hashCount + blockHeaderHashes + stopHash
    length = struct.pack("I", len(payload))
    suma_kontrolna = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]
    pakiet_danych = magic + str.encode(command) + length + suma_kontrolna + payload
    socket.send(pakiet_danych)
    return


def Pobierz_naglowki(socket, version, pierwszy_hash):
    magic = codecs.decode('F9BEB4D9', 'hex')
    command = "getheaders" + (12 - len("getheaders")) * "\00"
    hashCount = struct.pack("B", 1)
    blockHeaderHashes = pierwszy_hash
    ostatni_hash = b'\0' * 32
    payload = version + hashCount + blockHeaderHashes + ostatni_hash
    length = struct.pack("I", len(payload))
    suma_kontrolna = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]
    pakiet_danych = magic + str.encode(command) + length + suma_kontrolna + payload
    socket.send(pakiet_danych)
    return


def intToCompactSizeInt(input):
    if (input > 252):
        inBytes = bytes.fromhex("fd")
        print("Int codec begin")
        print(inBytes)
        print("Input number in little endian bytes")
        print(struct.pack('<I', input))
        numberInBytes = struct.pack('<I', input)
        inBytes = inBytes + numberInBytes[:2]
        print("The final number in compactSizeInt")
        print(inBytes)
        return inBytes
    else:
        print("Input number in little endian bytes")
        print(struct.pack('<I', input))
        numberInBytes = struct.pack('<I', input)
        inBytes = numberInBytes[:1]
        print("The final number in compactSizeInt")
        print(inBytes)
        return inBytes


def wysylanie_ping(socket):
    magic = codecs.decode('F9BEB4D9', 'hex')
    command = "ping" + (12 - len("ping")) * "\00"
    payload = codecs.decode('d9c3245148899faf', 'hex')
    length = struct.pack("I", len(payload))
    suma_kontrolna = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]
    pakiet_danych = magic + str.encode(command) + length + suma_kontrolna + payload
    socket.send(pakiet_danych)

    print("\n--------------Wysy?anie pakietu Ping---------------------\n")
    print("Magic:", magic.hex())
    print("Command:", command)
    length = int.from_bytes(length, byteorder='little')
    print("Payload Lenght:", length)
    print("Checksum:", suma_kontrolna.hex())
    print("Nonce:", payload.hex(),"\n")

    return


def wysylanie_pong(socket, nonce):
    magic = codecs.decode('F9BEB4D9', 'hex')
    command = "pong" + (12 - len("pong")) * "\00"
    payload = nonce
    length = struct.pack("I", len(payload))
    suma_kontrolna = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]
    pakiet_danych = magic + str.encode(command) + length + suma_kontrolna + payload
    socket.send(pakiet_danych)

    print("\n--------------Wysy?anie pakietu Pong---------------------\n")
    print("Magic:", magic.hex())
    print("Command:", command)
    length = int.from_bytes(length, byteorder='little')
    print("Payload Lenght:", length)
    print("Checksum:", suma_kontrolna.hex())
    print("Nonce:",nonce.hex())

    return

def wypisanie_pakietu(magic,command,length,suma_kontrolna,version,services):
    print("--------------Wysy?anie pakietu Version---------------------\n")
    print("Magic:", magic.hex())
    print("Command:", command)

    services = int.from_bytes(services, byteorder='little')
    length = int.from_bytes(length, byteorder='little')
    version = int.from_bytes(version, byteorder='little')

    print("Payload Lenght:", length)
    print("Checksum:", suma_kontrolna.hex())
    print("Payload:\n   Version:", version, "\n   Services:", services, "\n   ...\n")
    return

def wypisanie_pakietu_verack(magic,command,length,suma_kontrolna):
    print("\n--------------Wysy?anie pakietu Verack---------------------\n")
    print("Magic:", magic.hex())
    print("Command:", command)

    length = int.from_bytes(length, byteorder='little')

    print("Payload Lenght:", length)
    print("Checksum:", suma_kontrolna.hex())
    return

Pierwszy_blok_hash = b'\00\00\00\00\00\19\d6\68\9c\08\5a\e1\65\83\1e\93\4f\f7\63\ae\46\a2\a6\c1\72\b3\f1\b6\0a\8c\e2\6f'
payload = Version.version + Version.services + Version.timestamp + Version.addr_recv_services + Version.addr_recv_ip + Version.addr_recv_port + Version.addr_trans_services + Version.addr_trans_ip + Version.addr_trans_port + Version.nonce + Version.user_agent_bytes + Version.starting_height + Version.relay #ciag bajtow

###############################################################TWORZENIIE NAG??WKA

magic = codecs.decode('F9BEB4D9', 'hex') #czy dany pakiet pochodzi z sieci Bitcoin. Kodowanie wartosci w systemie szestanstkowym
command = "version" + 5 * "\00"  #\00 means null in python, pole ma 12 bajtow, jesli wiadomosc jest kr?tsza wtedy uzupelniana jest zerami
length = struct.pack("I", len(payload)) # wartosc okreslajaca liczbe bajtow wiadomosci do zinterpretowania(payload)
suma_kontrolna = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4] #podwojne wyliczanie sumy kontrolnej

pakiet_danych = magic + str.encode(command) + length + suma_kontrolna + payload #stworzenie pakietu bedac gotowym do wyslania w siec

socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #tworzenie gniazda
adres_sieci_bitcoin = "5.9.60.6"  # 185.165.76.220 Adres IP sieci bitcon
port = 8333 #standrdowy port sieci bitcoin
socket.connect((adres_sieci_bitcoin, port)) # ?aczenie sie z sieci?
socket.send(pakiet_danych) #wyslanie pakietu danych do siec bitcoin

wypisanie_pakietu(magic,command,length,suma_kontrolna,Version.version,Version.services)

odpowiedz_sieci_bitcoin = Odczyt_naglowka_z_socketu(socket)
h = analizanaglowka(odpowiedz_sieci_bitcoin)

payload = socket.recv(h.length)  # recive bytes length of payload
print("Payload:",payload.hex())
peerVersion = Payload()
peerVersion.header = h
peerVersion.payload = payload

pakiet_danych = magic + str.encode(Verack.command) + Verack.length + Verack.suma_kontrolna + Verack.payload #wysyla
socket.send(pakiet_danych)
wypisanie_pakietu_verack(magic,command,length,suma_kontrolna)

response = Odczyt_naglowka_z_socketu(socket)
h = analizanaglowka(response)

payload = socket.recv(h.length)  # recive bytes length of payload
print("payload",payload.hex())
peerVerack = Payload()
peerVerack.header = h
peerVerack.payload = payload
for i in range(
        6):  # pobierzmy 6 pierwszych wiadomo?ci, kt?re s? zawsze wysy?ane. Jedna z nich to ping, na kt?ry prawdopodobnie trzeba odpowiedzie?
    response = Odczyt_naglowka_z_socketu(socket)
    h = analizanaglowka(response)
    payload = socket.recv(h.length)  # recive bytes length of payload
    print("Payload",payload.hex())
    payloadStr = payloadInterpreter(payload, h.command)
    for i in payloadStr:
        print(i)
    if h.command == 'ping':
        wysylanie_pong(socket, payload)

while 1:
    print("\nWybierz jedn? z dost?pnych opcji:")
    print("1. Pobierz adresy (getaddr - addr)\n2. Wysy?anie pinga (ping - pong)\n3. Pobierz nag??wki (getheaders - headers)\n4. Pobierz bloki(getblocks - inv)n")
    wybor = input()
    commandName = ''
    numberOfData = 1
    if (wybor == '1'):  # wyszukiwanie aktywnych peer?w
        Pobierz_adresy_wezlow(socket)
    if (wybor == '2'): # informacje o konkretnych transakcjach lub ich blokach.
        wysylanie_ping(socket)
    if (wybor == '3'):
        Pobierz_naglowki(socket, Version.version, Pierwszy_blok_hash)
    if (wybor == '4'):
        Pobierz_bloki(socket, magic, Version.version, Pierwszy_blok_hash)
        #numberOfData = getData(socket, magic)

    for j in range(int(numberOfData)):
        response = Odczyt_naglowka_z_socketu(socket)
        h = analizanaglowka(response)
        payload = socket.recv(h.length)  # recive bytes length of payload
        if (len(payload) > h.length):
            print("Error: Recieved too much payload")
        if (len(payload) < h.length):
            #print("Recieved not enought payload")
            #print("Payload before addition")
           # print(payload)
            while len(payload) < h.length:
                numberOfBytesLeft = h.length - len(payload)
                addition = socket.recv(numberOfBytesLeft)
                additionInString = addition.hex()
                payloadInString = payload.hex()
                payloadInString += additionInString
                payload = bytes.fromhex(payloadInString)
                #print("Payload length after addition")
               # print(len(payload))
                #print("Payload after addition")
               # print(payload)

        payload = payloadInterpreter(payload, h.command)
        for i in payload:
            print(i)
        if (h.command == 'inv'):
            myBlockHeaders = payload[1:]


